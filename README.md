# README #

This python script converts panoramic images from the standard [latlong format](https://en.wikipedia.org/wiki/Equirectangular_projection) to a [cube map](https://en.wikipedia.org/wiki/Cube_mapping).

### How do I get set up? ###

* pip install numpy
* python panoFaces.py yourPanoramicImage.jpg

### Who do I talk to? ###

* Send suggestions and comments to panofaces@mantzel.org
